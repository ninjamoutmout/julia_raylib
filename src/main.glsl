#version 400 core

in vec2 fragTexCoord;
in vec4 fragColor;

out vec4 color;

uniform vec3 cam;

uniform int max_iter;
uniform int fractal_num;
uniform float biase;

struct Complex {
	double real;
	double imag;
};

uniform vec2 resolution;


int mandelbrot(in Complex c, in int max_iter) {

	Complex z = Complex(
		0.0,
		0.0
	);

	for (int i = 0; i < max_iter; i++) {
		if (z.real*z.real + z.imag*z.imag >= 4.0) { // euclidian distance >= 2
            return i;
        }

		// (a + bi)^2
		// a^2 + 2abi - b^2
		z = Complex(
			z.real*z.real - z.imag*z.imag,
			2.0*z.real*z.imag
		);

		z.real += c.real; 
		z.imag += c.imag;
		// z.real += 0.285;
		// z.imag += 0.01;
	}

    return max_iter;
}

int julia(in Complex z, in float b, in int max_iter) {

	for (int i = 0; i < max_iter; i++) {
		if (z.real*z.real + z.imag*z.imag >= 4.0) { // euclidian distance >= 2
            return i;
        }

		// (a + bi)^2
		// a^2 + 2abi - b^2
		z = Complex(
			z.real*z.real - z.imag*z.imag,
			2.0*z.real*z.imag
		);

		z.real += 0.7885*cos(b); //c is -0.512 + -0.512i, part of mandelbrot
		z.imag += 0.7885*sin(b);
		// z.real += 0.285;
		// z.imag += 0.01;
	}

    return max_iter;
}

vec4 coloring(in int i){
	float value = float(i) / float(max_iter) * 100.0;
	if (i == max_iter) {return vec4(0.0, 0.0, 0.0, 1.0);}
	else if (value >= 95.0){return vec4(1.0, 0.0, 0.45, 1.0);}
	else if (value >= 90.0){return vec4(1.0, 0.0, 0.56, 1.0);}
	else if (value >= 85.0){return vec4(1.0, 0.0, 0.68, 1.0);}
	else if (value >= 80.0){return vec4(1.0, 0.0, 0.80, 1.0);}

	else if (value >= 75.0){return vec4(0.82, 0.0, 1.0, 1.0);}
	else if (value >= 70.0){return vec4(0.58, 0.0, 1.0, 1.0);}
	else if (value >= 65.0){return vec4(0.431, 0.0, 1.0, 1.0);}

	else if (value >= 60.0){return vec4(0.254, 0.0, 1.0, 1.0);}
	else if (value >= 55.0){return vec4(0.039, 0.0, 1.0, 1.0);}
	else if (value >= 50.0){return vec4(0.0, 0.52, 1.0, 1.0);}
	else if (value >= 45.0){return vec4(0.0, 0.65, 1.0, 1.0);}
	else if (value >= 40.0){return vec4(0.0, 0.83, 1.0, 1.0);}

	else if (value >= 35.0){return vec4(0.0, 1.0, 1.0, 1.0);}
	else if (value >= 30.0){return vec4(0.0, 1.0, 0.55, 1.0);}
	else if (value >= 25.0){return vec4(0.0, 1.0, 0.0, 1.0);}
	else if (value >= 22.5){return vec4(0.69, 1.0, 0.0, 1.0);}
	else if (value >= 20.0){return vec4(0.87, 1.0, 0.0, 1.0);}
	else if (value >= 17.5){return vec4(1.0, 0.87, 0.0, 1.0);}
	else if (value >= 15.0){return vec4(1.0, 0.58, 0.0, 1.0);}
	else if (value >= 12.5){return vec4(1.0, 0.39, 0.0, 1.0);}
	else if (value >= 11.25){return vec4(1.0, 0.29, 0.0, 1.0);}
	else if (value >= 10.0){return vec4(1.0, 0.196, 0.0, 1.0);}
	else if (value >= 7.5){return vec4(1.0, 0.096, 0.0, 1.0);}
	else if (value >= 5.0){return vec4(1.0, 0.0, 0.0, 0.95);}
	else if (value >= 2.5){return vec4(1.0, 0.0, 0.0, 0.9);}

	else {
		return vec4(255.0, 0.0, 0.0, 0.8);
		// color = vec4(255.0, 0.0, 0.0, 1.0);
	}

}

void main(){
	// int max_iter = 50;
	vec2 p = gl_FragCoord.xy / resolution; 

	if (fractal_num == 0) {

		const double scaling = 2 << 16;
		dvec3 dcam = dvec3(cam) / scaling;

		Complex c = Complex(
		double(p.x - 0.5) * dcam.z + dcam.x,
		double((p.y - 0.5) * resolution.y/resolution.x) * dcam.z + dcam.y
		);


		int i = 0;
		i = mandelbrot(c, max_iter);

		color = coloring(i);

	}
	else if (fractal_num == 1) {
		
		const double scaling = 2 << 16;
		dvec3 dcam = dvec3(cam) / scaling;

		Complex z = Complex(
			double(p.x - 0.5) * dcam.z + dcam.x,
			double((p.y - 0.5) * resolution.y/resolution.x) * dcam.z + dcam.y
		);


		int i = 0;
		i = julia(z, biase, max_iter);

		color = coloring(i);

	}
	
	

}
