use std::{ffi::CString, f32::consts::PI};
use raylib::prelude::*;
use std::io;


#[repr(C)]
pub struct Complex(FP, FP);

type FP = f64;
type Uniform = i32;

struct Camera {
    x: FP, y: FP,
    zoom: FP
}

pub struct FractalShader{
    s: Shader,
    resolution: Uniform,
    biase: Uniform,
    max_iter: Uniform,
    fractal_num: Uniform,
    cam: Uniform,
}

impl FractalShader {
    fn create(rl: &mut RaylibHandle, thread: &RaylibThread) -> Self {
        const SHADER: &str = include_str!("main.glsl");
        let s = rl.load_shader_from_memory(thread, None, Some(SHADER));
        let resolution = s.get_shader_location("resolution");
        let biase = s.get_shader_location("biase");
        let max_iter = s.get_shader_location("max_iter");
        let fractal_num = s.get_shader_location("fractal_num");
        let cam = s.get_shader_location("cam");
        Self {
            s, resolution, biase, max_iter, fractal_num, cam
        }
    }
}

fn main() {

    let mut max_iter_exp: i32 = 6;
    let mut max_iter = 2_i32.pow(max_iter_exp as u32);

    let (mut rl, thread) = raylib::init()
        .size(1280, 960)
        .title("Julia set")
        .resizable()
        .build();
    
    let mut cam = Camera {
        x: -0.5, y: 0.0,
        zoom: 2.5
    };



    let mut shader: FractalShader = FractalShader::create(&mut rl, &thread);

    shader.s.set_shader_value(shader.max_iter, max_iter);
    shader.s.set_shader_value::<[f32; 2]>(shader.resolution, [1280.0, 960.0]);
    shader.s.set_shader_value(shader.fractal_num, 1);



    let mut animating = true;
    let mut biase: f32 = 0.0;
    let mut rerender = true;

    let mut target = rl.load_render_texture(&thread, 1280, 960).unwrap();

    rl.set_target_fps(120);


    while !rl.window_should_close(){
    
        let (w, h) = (rl.get_screen_width(), rl.get_screen_height());

        if rl.is_window_resized() {
            shader.s.set_shader_value::<[f32; 2]>(shader.resolution, [w as f32, h as f32]);
            target = rl.load_render_texture(&thread, w as _, h as _).unwrap();
            rerender = true;
        }

        match rl.get_mouse_wheel_move() {
            y if y != 0.0 => {
                let Vector2 { x: mouse_x, y: mouse_y } = rl.get_mouse_position();

                let delta = y as FP / 10.0;
                cam.x += cam.zoom*delta*(mouse_x as FP / w as FP - 0.5);
                cam.y += cam.zoom*delta*(mouse_y as FP / w as FP - (h as FP / w as FP/2.0));
                cam.zoom *= 1.0 - delta;
                rerender = true; 
            }
            _ => ()
        }

        if rl.is_mouse_button_down(MouseButton::MOUSE_BUTTON_LEFT) {
            let delta = rl.get_mouse_delta();
            if delta != Vector2::zero() {
                cam.x -= cam.zoom*delta.x as FP / w as FP;
                cam.y -= cam.zoom*delta.y as FP / w as FP;
                rerender = true;
            }
        }

        if animating && rl.is_window_focused(){
            biase += 0.002615;
            rerender = true;
        }

        let mut d = rl.begin_drawing(&thread);
        d.clear_background(Color::new(0, 0, 0, 0));

        if rerender{
            {
                let mut d = d.begin_texture_mode(&thread, &mut target);
                {
                    let scaling = 2.0_f64.powi(16);

                    shader.s.set_shader_value(shader.biase, biase);
                    shader.s.set_shader_value(shader.cam, [(cam.x * scaling) as f32, (cam.y * scaling) as f32, (cam.zoom * scaling) as f32]);
                    
                    let mut d = d.begin_shader_mode(&shader.s);
                    d.draw_rectangle(0, 0, w, h, Color::WHITE);

                }
            rerender = false;
                
            }

        }
        d.draw_texture(&target, 0, 0, Color::WHITE);
        // thread::sleep(Duration::new(0, 1_000_000_000u32 / 60));
        if d.gui_button(Rectangle::new(5.0, 5.0, 65.0, 20.0), Some(CString::new("Mandelbrot").unwrap().as_c_str())){
            shader.s.set_shader_value(shader.fractal_num, 0);
            animating = false;
            rerender = true;
        }
        if d.gui_button(Rectangle::new(75.0, 5.0, 65.0, 20.0), Some(CString::new("Julia").unwrap().as_c_str())){
            shader.s.set_shader_value(shader.fractal_num, 1);
            biase = 0.0;
            animating = true;
            rerender = true;
        }
        if d.gui_button(Rectangle::new(145.0, 5.0, 65.0, 20.0), Some(CString::new("Pause").unwrap().as_c_str())){
            animating = !animating;
        }

        if d.gui_button(Rectangle::new(215.0, 5.0, 65.0, 20.0), Some(CString::new("max iter -").unwrap().as_c_str())){
            max_iter_exp += -1;
            max_iter = 2_i32.pow(max_iter_exp as u32);
            shader.s.set_shader_value(shader.max_iter, max_iter);
            rerender = true;

        }
        if d.gui_button(Rectangle::new(285.0, 5.0, 65.0, 20.0), Some(CString::new("max iter +").unwrap().as_c_str())){
            max_iter_exp += 1;
            max_iter = 2_i32.pow(max_iter_exp as u32);
            shader.s.set_shader_value(shader.max_iter, max_iter);
            rerender = true;
        }
    }
}
